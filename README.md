# Serial Message

C90 Utility library to send message over serial communication

It allows sending raw binary data over serial with error checking

## Usage

### Sender

* `serial_message_make_packet`: convert a byte array into a packet ready to be sent

### Receiver

* `serial_message_accumulate_byte`: accumulate and reconstruct a packet from a single byte
* `serial_message_accumulate_buffer`: accumulate and reconstruct a packet from a byte array
* `serial_message_decode_message`: decode message from a received packet

### Examples

an example is provided in the `examples` folder to showcase how to use the library

## Packet

A packet is composed by signaling bytes (orange), headers fields (blue), a payload (green) and a CRC (red)
![packet](doc/resource/packet.drawio.png)

signaling bytes are standard ASCII field for packet structure.

headers fields are message ID (unused) and payload size.

Payload size is use for checking packet integrity

Payload is the serialized message provided by user

CRC is a CRC16 for checking packet integrity.



