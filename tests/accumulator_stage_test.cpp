/**
 * @file    accumulator_stage_test.cpp
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "../src/accumulator_stage.h"
#include <gtest/gtest.h>
#include "../src/utils.h"
#include "serial_message.h"

TEST(accumulator_stage, stage_soh) {
    {
        // valid
        unsigned char buffer[10] = {};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 0,
        };
        EXPECT_EQ(smu_serial_message_accumulator_stage_soh(SOH, &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator.length, 1);
        EXPECT_EQ(accumulator.buffer[0], SOH);
    }
    {
        // ignore char
        unsigned char buffer[10] = {};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 0,
        };
        for (int i = 0; i <= 127; ++i) {
            if (i == SOH) {
                continue;
            }
            EXPECT_EQ(smu_serial_message_accumulator_stage_soh(i, &accumulator), SMAR_NO_MESSAGE);
            EXPECT_EQ(accumulator.length, 0);
        }
    }
}

TEST(accumulator_stage, stage_header) {
    {
        // valid
        unsigned char buffer[20] = {SOH};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 1,
                ._stage_handler = smu_serial_message_accumulator_stage_header,
        };

        // ID
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);

        // SIZE
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_header);

        // STX
        EXPECT_EQ(accumulator._stage_handler(STX, &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
    }
    {
        // Ill formed header
        unsigned char buffer[20] = {SOH};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 1,
                ._stage_handler = smu_serial_message_accumulator_stage_header,
        };
        for (int i = 0; i < PACKET_SIZE_HEADER; ++i) {
            EXPECT_EQ(accumulator._stage_handler(0, &accumulator), SMAR_ERROR_ILL_FORMED_HEADER);
            EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        }
    }
    {
        // Ill sized
        unsigned char buffer[20] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0'};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 9,
                ._stage_handler = smu_serial_message_accumulator_stage_header,
        };
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_ERROR_ILL_SIZED_HEADER);
    }
}

TEST(accumulator_stage, stage_payload) {
    {
        // valid
        // no payload
        unsigned char buffer[20] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 10,
                ._stage_handler = smu_serial_message_accumulator_stage_payload,
        };
        EXPECT_EQ(accumulator._stage_handler(ETX, &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_crc);

        // with payload
        buffer[6] = '4';
        accumulator.length = 10;
        accumulator._stage_handler = smu_serial_message_accumulator_stage_payload;
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
        EXPECT_EQ(accumulator._stage_handler(ETX, &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_crc);
        EXPECT_EQ(accumulator.length, 15);
    }
    {
        // Invalid payload size
        // Too small
        unsigned char buffer[20] = {SOH, '0', '0', '0', '0', '0', '2', '0', '0', STX};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 10,
                ._stage_handler = smu_serial_message_accumulator_stage_payload,
        };
        EXPECT_EQ(accumulator._stage_handler(ETX, &accumulator), SMAR_ERROR_ILL_SIZED_PAYLOAD);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);

        // too long
        accumulator.length = 10;
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler(ETX, &accumulator), SMAR_ERROR_ILL_SIZED_PAYLOAD);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
    }
    {
        // Invalid char
        unsigned char buffer[20] = {SOH, '0', '0', '0', '0', '0', '2', '0', '0', STX};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 10,
                ._stage_handler = smu_serial_message_accumulator_stage_payload,
        };
        EXPECT_EQ(accumulator._stage_handler(0, &accumulator), SMAR_ERROR_ILL_FORMED_PAYLOAD);
        EXPECT_EQ(accumulator._stage_handler, smu_serial_message_accumulator_stage_payload);
    }
}

TEST(accumulator_stage, stage_crc) {
    // CRC computed with http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
    {
        // valid
        // MSG: 0x01 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x02 0x03
        // CRC: 0x02AE
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX, ETX, '0', '0', '0', '0', '0'};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 11,
                ._stage_handler = smu_serial_message_accumulator_stage_crc,
        };
        EXPECT_EQ(accumulator._stage_handler('A', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler('E', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler('2', &accumulator), SMAR_NO_MESSAGE);
        EXPECT_EQ(accumulator._stage_handler(EOT, &accumulator), SMAR_HAS_MESSAGE);
    }
    {
        // ill sized crc
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX, ETX, 'A', 'E', '0', '1'};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 15,
                ._stage_handler = smu_serial_message_accumulator_stage_crc,
        };
        EXPECT_EQ(accumulator._stage_handler('0', &accumulator), SMAR_ERROR_ILL_SIZED_CRC);
        accumulator.length = 12;
        EXPECT_EQ(accumulator._stage_handler(EOT, &accumulator), SMAR_ERROR_ILL_SIZED_CRC);
    }
    {
        // invalid crc
        // expected crc: 0x02'AE
        // 0x01 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x02 0x03
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX, ETX, 'A', 'E', '0', '1'};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 15,
                ._stage_handler = smu_serial_message_accumulator_stage_crc,
        };
        EXPECT_EQ(accumulator._stage_handler(EOT, &accumulator), SMAR_ERROR_INVALID_CRC);
    }
    {
        // ill formed crc
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX, ETX, 'C', 'B', '5'};
        serial_message_accumulator_t accumulator = {
                .buffer = buffer,
                .length = 14,
                ._stage_handler = smu_serial_message_accumulator_stage_crc,
        };
        EXPECT_EQ(accumulator._stage_handler(0, &accumulator), SMAR_ERROR_ILL_FORMED_CRC);
    }
}
