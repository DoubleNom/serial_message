/**
 * @file    integration_test.cpp
 * @author  Paul Thomas
 * @date    5/26/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>
#include "../src/utils.h"
#include "serial_message.h"

TEST(integration, reception) {
    unsigned char buffer[100] = {};
    serial_message_accumulator_t accumulator;
    serial_message_init_accumulator(&accumulator, buffer, 100);
    // MSG: 0x01 0x30 0x30 0x30 0x30 0x30 0x34 0x30 0x30 0x02 0x41 0x42 0x43 0x44 0x03
    // CRC: 0xB493  -> '9','3', 'B', '4'
    unsigned char message[256] = {SOH, '0', '0', '0', '0', '0', '4', '0', '0', STX,
                                  'A', 'B', 'C', 'D', ETX, '9', '3', 'B', '4', EOT};
    unsigned short message_length = 20;
    for (int i = 0; i < message_length; ++i) {
        auto c = message[i];
        auto result = serial_message_accumulate_byte(c, &accumulator);
        EXPECT_EQ(result, c == EOT ? SMAR_HAS_MESSAGE : SMAR_NO_MESSAGE);
    }

    serial_message_reset_accumulator(&accumulator);

    auto result = serial_message_accumulate_buffer(message, &message_length, &accumulator);
    EXPECT_EQ(message_length, 20);
    EXPECT_EQ(result, SMAR_HAS_MESSAGE);

    serial_message_reset_accumulator(&accumulator);
    message_length = 8;
    result = serial_message_accumulate_buffer(message, &message_length, &accumulator);
    EXPECT_EQ(message_length, 8);
    EXPECT_EQ(result, SMAR_NO_MESSAGE);
    message_length = 8;
    result = serial_message_accumulate_buffer(message + 8, &message_length, &accumulator);
    EXPECT_EQ(message_length, 8);
    EXPECT_EQ(result, SMAR_NO_MESSAGE);
    message_length = 255;
    result = serial_message_accumulate_buffer(message + 16, &message_length, &accumulator);
    EXPECT_EQ(message_length, 4);
    EXPECT_EQ(result, SMAR_HAS_MESSAGE);
}

TEST(integration, make_packet) {
    {
        // valid
        unsigned char payload[] = {0xAB, 0xCD};
        unsigned char computed[256] = {};
        unsigned short computed_length = 256;
        unsigned char expected[] = {SOH, '0', '0', '0', '0', '0', '4', '0', '0', STX,
                                    'A', 'B', 'C', 'D', ETX, '9', '3', 'B', '4', EOT};
        unsigned short expected_length = 20;
        auto result = serial_message_make_packet(payload, 2, computed, &computed_length);
        EXPECT_EQ(result, SMCR_SUCCESS);
        EXPECT_EQ(computed_length, expected_length);
        for (int i = 0; i < computed_length; ++i) {
            EXPECT_EQ(computed[i], expected[i]);
        }
    }
    {
        // insufficient space
        unsigned short length = 3;
        auto result = serial_message_make_packet(nullptr, 2, nullptr, &length);
        EXPECT_EQ(result, SMCR_ERROR_BUFFER_TOO_SMALL);
    }
}

TEST(integration, decode_message) {
    {
        // valid
        unsigned char accumulator_buffer[] = {SOH, '0', '0', '0', '0', '0', '4', '0', '0', STX,
                                              'A', 'B', 'C', 'D', ETX, '9', '3', 'B', '4', EOT};
        serial_message_accumulator_t accumulator{
                .buffer = accumulator_buffer,
                .length = 20,
        };
        unsigned char message_buffer[2] = {};
        unsigned short message_buffer_length = 2;
        auto result = serial_message_decode_message(&accumulator, message_buffer, &message_buffer_length);
        EXPECT_EQ(result, SMCR_SUCCESS);
        EXPECT_EQ(message_buffer_length, 2);
        EXPECT_EQ(message_buffer[0], 0xAB);
        EXPECT_EQ(message_buffer[1], 0xCD);
    }
    {
        // invalid crc
        unsigned char accumulator_buffer[] = {SOH, '0', '0', '0', '0', '0', '4', '0', '0', STX,
                                              'A', 'B', 'C', 'D', ETX, '2', '3', '4', '1', EOT};
        serial_message_accumulator_t accumulator{
                .buffer = accumulator_buffer,
                .length = 20,
        };
        unsigned char message_buffer[2] = {};
        unsigned short message_buffer_length = 2;
        auto result = serial_message_decode_message(&accumulator, message_buffer, &message_buffer_length);
        EXPECT_EQ(result, SMCR_ERROR_INVALID_ACCUMULATOR);
    }
    {
        // invalid size
        unsigned char accumulator_buffer[] = {SOH, '0', '0', '0', '0', '0', '6', '0', '0', STX,
                                              'A', 'B', 'C', 'D', ETX, 'F', '4', '7', '2', EOT};
        serial_message_accumulator_t accumulator{
                .buffer = accumulator_buffer,
                .length = 20,
        };
        unsigned char message_buffer[2] = {};
        unsigned short message_buffer_length = 2;
        auto result = serial_message_decode_message(&accumulator, message_buffer, &message_buffer_length);
        EXPECT_EQ(result, SMCR_ERROR_INVALID_ACCUMULATOR);
    }
    {
        // buffer too small
        unsigned char accumulator_buffer[] = {SOH, '0', '0', '0', '0', '0', '4', '0', '0', STX,
                                              'A', 'B', 'C', 'D', ETX, '9', '3', 'B', '4', EOT};
        serial_message_accumulator_t accumulator{
                .buffer = accumulator_buffer,
                .length = 20,
        };
        unsigned char message_buffer[2] = {};
        unsigned short message_buffer_length = 1;
        auto result = serial_message_decode_message(&accumulator, message_buffer, &message_buffer_length);
        EXPECT_EQ(result, SMCR_ERROR_BUFFER_TOO_SMALL);
    }
}

TEST(integration, full_pass) {
    unsigned char src_message_buffer[] = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7,
                                          0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF};
    unsigned short src_message_buffer_length = 16;
    unsigned char src_packet_buffer[256] = {};
    unsigned short src_packet_buffer_length = 256;

    serial_message_make_packet(src_message_buffer, src_message_buffer_length, src_packet_buffer,
                               &src_packet_buffer_length);

    unsigned char accumulator_buffer[256] = {};
    serial_message_accumulator_t accumulator;
    serial_message_init_accumulator(&accumulator, accumulator_buffer, 256);

    serial_message_accumulate_buffer(src_packet_buffer, &src_packet_buffer_length, &accumulator);

    unsigned char dst_message_buffer[256] = {};
    unsigned short dst_message_buffer_length = 256;
    serial_message_decode_message(&accumulator, dst_message_buffer, &dst_message_buffer_length);

    EXPECT_EQ(src_message_buffer_length, dst_message_buffer_length);
    for (int i = 0; i < src_message_buffer_length; ++i) {
        EXPECT_EQ(src_message_buffer[i], dst_message_buffer[i]);
    }
}
